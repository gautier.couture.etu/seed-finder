#Author : Gautier COUTURE
from Structure import *
from tqdm import tqdm
from Bio import SeqIO
from Bio.Seq import Seq
import argparse, sys, parasail, gzip

def getGenome(genomeFile):
    '''
    Return the genome sequences of a fasta file in a string format

    Parameters:
        genomeFile (str): Path to a fasta file that contain a genome

    Returns:
        myGenome (str): String that contain all the genome genome of the given file
    '''
    myGenome = []
    with open(genomeFile, 'r') as file:
        for line in SeqIO.parse(file, "fasta"):
            myGenome.append(line.seq)
        file.close()
    return myGenome

def createSuffixArray(genome, kmerSize):
    '''
    Create a suffix table in relation with a given genome

    Parameters:
        genome (str): Path to a fasta file that contain a genome
        kmerSize (int): The value of k for which to count k-mers

    Returns:
        suffixArray (list): List of int which represent the lexicographical sort of the genome suffixes
    '''
    suffixArray = list(range(len(genome) - kmerSize))
    suffixArray.sort(key = lambda i: genome[i:])
    return suffixArray

def getReadsAndAlign(readFile, kmerSize, genome, suffixArray):
    '''
    Return a list of reads that have been seeded, extended and selected by their score

    Parameters:
        readFile (str): Path to a fastq file that contain reads
        kmerSize (int): The value of k for which to count k-mers
        genome (str): Path to a fasta file that contain a genome
        suffixArray (list): List of int which represent the lexicographical sort of the genome suffixes

    Returns:
        readList (list): List of readStruct objects that contain the informations of each aligned reads
    '''
    logEveryN = 10000
    count = 0
    maxScore = 0
    minimalRequiredScore = 285
    readList = []
    res = {}
    for i in range(0,len(readFile)):
        with gzip.open(readFile[i], "rt") as file:
            for read in SeqIO.parse(file, "fastq"):
                count += 1
                if (count % logEveryN ) == 0:
                    print(str(count)+" reads treated", end='\r')
                if read.seq[0:10] != 'NNNNNNNNNN': #Filter the NNNNNN-like reads
                    kmers = getKmers(read, kmerSize)
                    seeds = findSeeds(genome, suffixArray, kmers, kmerSize)
                    for seed in seeds.values():
                        for data in seed:
                            startPos = max(data - (len(read.seq)//2), 0)
                            endPos = min(data + kmerSize + (len(read.seq)//2), len(genome))
                            myAlignment = parasail.sg_dx_trace_scan(str(read.seq), str(genome[startPos:endPos]), 10, 1, parasail.dnafull)
                            if myAlignment.score >= minimalRequiredScore and myAlignment.score >= maxScore:
                                maxScore = myAlignment.score
                                maxAlignment = myAlignment
                                supposedStart = data
                    if maxScore >= minimalRequiredScore:
                        readList.append(readStruct(read.id,supposedStart,maxScore,read.seq,maxAlignment.cigar.decode))
                        maxScore = 0
                        supposedStart = 0
            file.close()
    return readList

def getKmers(read, kmerSize):
    '''
    Find kmer in a given read and filter them (here the filter is 10).

    Parameters:
        read (string): A single DNA genome.
        kmerSize (int): The value of k for which to count k-mers

    Returns:
        kmerList (list): A list of k-mers of a read
    '''
    kmerList = []
    reverseComplement = read.reverse_complement()
    kmerCount = len(read) - kmerSize + 1 #Calculate how many kmers of length k there are
    for i in range(0,kmerCount, 10):
        kmerList.append(read[i:i+kmerSize])
        kmerList.append(reverseComplement[i:i+kmerSize])
    return kmerList

def findSeeds(genome, suffixArray, kmers, kmerSize):
    '''
    Return a dictionnary of seeds found in the given genome

    Parameters:
        genome (str): Path to a fasta file that contain a genome
        suffixArray (list): List of int which represent the lexicographical sort of the genome suffixes
        kmers (list): A list of k-mers of a read
        kmerSize (int): The value of k for which to count k-mers

    Returns:
        seeds (dict): Dictionnary of seeds
    '''
    seeds = {}
    for i in range(len(kmers)):
        kmer = kmers[i].seq
        firstKmer, lastKmer = binarySeedFinder(genome, suffixArray, kmer, kmerSize)
        for j in range(firstKmer, lastKmer+1):
            if(seeds.get(kmer, 0) != 0):
                seeds[kmer].append(suffixArray[j])
            else:
                seeds[kmer] = [suffixArray[j]]
    return seeds

def binarySeedFinder(genome, suffixArray, kmer, kmerSize):
    '''
    Return the postion of the first and the last apparition of a given kmer in the genome

    Parameters:
        genome (str): Path to a fasta file that contain a genome
        suffixArray (list): List of int which represent the lexicographical sort of the genome suffixes
        kmer (str): A kmer
        kmerSize (int): The value of k for which to count k-mers

    Returns:
        low, high (int, int): First and last postion of a kmer in the genome
    '''
    low = 0
    high = len(suffixArray) - 1
    while low <= high:
        mid = low + (high - low) // 2
        if genome[suffixArray[mid]:suffixArray[mid]+kmerSize] == kmer and genome[suffixArray[mid-1]:suffixArray[mid-1]+kmerSize] < kmer:
            low = mid
            break
        elif genome[suffixArray[mid]:suffixArray[mid]+kmerSize] < kmer:
            low = mid + 1
        else:
            high = mid -1
    start = low
    high = len(suffixArray)-1
    while start <= high:
        mid = start + (high - start) // 2
        if genome[suffixArray[mid]:suffixArray[mid]+kmerSize] == kmer and mid == len(suffixArray) -1:
            high = mid
            break
        elif genome[suffixArray[mid]:suffixArray[mid]+kmerSize] == kmer and genome[suffixArray[mid+1]:suffixArray[mid+1]+kmerSize] > kmer:
            high = mid
            break
        elif genome[suffixArray[mid]:suffixArray[mid]+kmerSize] < kmer:
            start = mid + 1
        else:
            high = mid -1
    return (low, high)

def writeOutFile(alignReads, outFileName):
    '''
    Write a file that contain all the reads we achieved to align with the reference genome

    Parameters:
        alignReads (list): List of readStruct (reads) we achieved to align with the reference genome
        outFileName (str): Name of the out file

    Returns:
        None
    '''
    with open(outFileName, "w+") as file :
        file.write("Read_Name\tStart\tScore\tSequence\tCIGAR\n")
        for i in tqdm(range (0, len(alignReads)), desc="Writing..."):
            if alignReads[i] != None :
                file.write(str(alignReads[i].get_id())+"\t"+str(alignReads[i].get_start())+"\t"+str(alignReads[i].get_score())+"\t"+str(alignReads[i].get_seq())+"\t"+str(alignReads[i].get_cigar())+"\n")
        file.close()

def runProcess(genome, reads, out, kmersize):
    '''
    Run the process in a fixed order in relation to instructions.

        Parameters:
            genome (str): Path to a fasta file that contain a genome
            reads (str): Path to aone or many files FASTQ that contain a genome
            out (str): Name of an out file
            kmers (int): Minimum size of kmers we need
        Returns:
            None
    '''
    print("-----START OF PROCESSING")
    print("Getting Genome...")
    myGenome = getGenome(genome) #myGenome is a string that contain the genome we're working on
    print("--Genome COLLECTED")
    print("Creating Suffix Array...")
    mySuffixArray = createSuffixArray(myGenome[0], kmersize)
    print("--Suffix Array CREATED")
    print("Getting Reads...")
    myAlignedReads = getReadsAndAlign(reads, kmersize, myGenome[0], mySuffixArray) #myReads is an array of reads that we're going to align with our genome
    print("--Reads COLLECTED")
    print("Writing Out File...")
    writeOutFile(myAlignedReads, out)
    print("--Out file WRITTEN")
    print("-----END OF PROCESSING")

def main():

    parser=argparse.ArgumentParser(prog="Seed_Finder.py",description="Programme de recherche et d'extention de seed")
    parser.add_argument("-g","--genome", metavar= '', help="Chemin vers un fichier FASTA contenant le génome de référence", required = True)
    parser.add_argument("-r","--reads", metavar= '',help="Chemin vers un ou plusieurs fichiers FASTQ.gz contenant le génome de référence", nargs='+',required = True)
    parser.add_argument("-o","--out", metavar= '',help="Fichier de sortie", required = True)
    parser.add_argument("-k","--kmersize", metavar= '',help="Taille des k-mers utilisés", default=5)
    args = parser.parse_args(sys.argv[1:])

    runProcess(args.genome, args.reads, args.out, int(args.kmersize))

if __name__=="__main__":
    main()
