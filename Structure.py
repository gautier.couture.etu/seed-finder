#Author : Gautier COUTURE
import sys

class readStruct :
    """
    A class to represent a read sequence.

    ...

    Attributes
    ----------
    id : str
        Id of the sequence
    start : int
        Approximate position of the beginning of the alignment on the reference genome
    score : int
        Score of the alignement
    seq : str
        Sequence of the fasta file
    cigar : str
        CIGAR of the alignement

    Methods
    -------
    get_id():
        Return the id attribute
    get_start():
        Return the start attribute
    get_score():
        Return the score attribute
    get_seq():
        Return the seq attribute
    get_cigar():
        Return the cigar attribute

    """
    def __init__(self,id,start,score,seq,cigar):
        """
        Constructs all the necessary attributes for the DnaSeq object.

        Parameters
        ----------
            id : str
                Id of the sequence
            start : int
                Approximate position of the beginning of the alignment on the reference genome
            score : int
                Score of the alignement
            seq : str
                Sequence of the fasta file
            cigar : str
                CIGAR of the alignement

        """
        self.id = id
        self.start = start
        self.score = score
        self.seq = seq
        self.cigar = cigar

    def get_id(self):
        """
        Returns the id attribute

        Parameters
        ----------
        None

        Returns
        -------
        id : str
            Id of the sequence
        """
        return self.id

    def get_start(self):
        """
        Returns the start attribute

        Parameters
        ----------
        None

        Returns
        -------
        description : int
            Approximate position of the beginning of the alignment on the reference genome
        """
        return self.start

    def get_score(self):
        """
        Returns the score attribute

        Parameters
        ----------
        None

        Returns
        -------
        description : int
            Score of the alignement
        """
        return self.score

    def get_seq(self):
        """
        Returns the seq attribute

        Parameters
        ----------
        None

        Returns
        -------
        seq : str
            Sequence of the fasta file
        """
        return self.seq

    def get_cigar(self):
        """
        Returns the cigar attribute

        Parameters
        ----------
        None

        Returns
        -------
        length : str
            CIGAR of the alignement
        """
        return self.cigar
