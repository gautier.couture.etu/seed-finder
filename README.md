Projet : Recherche et extention de seed

Auteur : Gautier COUTURE


Le projet s'éxecute grâce à la commande suivante :
  - Windows :
      py Seed_Finder.py -g *fichier_genome* -r *fichier_read* -o *nom_fichier_sortie* -k *taille-k-mers*  
  - Linux :
      python3 Seed_Finder.py -g *fichier_genome* -r *fichier_read* -o *nom_fichier_sortie* -k *taille-k-mers*  

Afficher les paramètres du programme :
- Windows :
    py Seed_Finder.py -h
- Linux :
    python3 Seed_Finder.py -h

Paramètres du programme :  
-h, --help      show this help message and exit  
-g , --genome   Chemin vers un fichier FASTA ontenant le génome de référence  
-r , --reads    Chemin vers un ou plusieurs fichiers FASTQ.gz contenant le génome de référence  
-o , --out      Fichier de sortie  
-k , --kmers    Taille des k-mers utilisés (Optionnel)  
